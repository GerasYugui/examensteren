package com.gerardot.gestiontickets.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.gerardot.gestiontickets.R
import com.gerardot.gestiontickets.model.Error
import kotlinx.android.synthetic.main.fragment_generico_pasos.*

/**
 * Created by Gerardo on 13/01/2020.
 */
class FragmentPaso1 : Fragment(){

    private lateinit var title: String
    private lateinit var descripcipn: String

    fun newInstance(error: Error) : FragmentPaso1{
        var bundle: Bundle = Bundle()
        bundle.putString("tipoError", error.tipoError.toString())
        bundle.putString("descripcion", error.paso1)

        var fragmentPaso1: FragmentPaso1 = FragmentPaso1()
        fragmentPaso1.arguments = bundle
        return  fragmentPaso1
    }

    private fun readBundle(bundle: Bundle){
        title = bundle.getString("tipoError").toString()
        descripcipn = bundle.getString("descripcion").toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_generico_pasos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.let { readBundle(it) }
        tv_fragment_name.text = title
        fragment_descripcion.text = descripcipn
    }
}