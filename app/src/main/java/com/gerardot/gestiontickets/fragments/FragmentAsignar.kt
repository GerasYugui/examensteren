package com.gerardot.gestiontickets.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Message
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.gerardot.gestiontickets.MainActivity
import com.gerardot.gestiontickets.R
import com.gerardot.gestiontickets.model.Departamentos
import com.gerardot.gestiontickets.model.Error
import com.gerardot.gestiontickets.model.GestionError
import com.gerardot.gestiontickets.utils.*
import kotlinx.android.synthetic.main.fragment_asignar.*

/**
 * Created by Gerardo on 13/01/2020.
 */
class FragmentAsignar : Fragment(), NetHelper.OnDataResultInterface{

    private lateinit var netHelper: NetHelper

    private lateinit var title: String
    private lateinit var descripcion: String
    private lateinit var id: String
    private lateinit var idTicket: String

    private var deptosList: MutableList<Departamentos> = ArrayList()
    private var deptosListNombres: MutableList<String> = ArrayList()
    private var deptosListIds: MutableList<String> = ArrayList()

    private var selectedDepto: String = ""

    fun newInstance(error: Error) : FragmentAsignar{
        var bundle: Bundle = Bundle()
        bundle.putString("tipoError", error.tipoError.toString())
        bundle.putString("descripcion", error.Descripcion)
        bundle.putString("idError", error.id)
        bundle.putString("idTicket", error.idTicket)


        var fragmentAsignar: FragmentAsignar = FragmentAsignar()
        fragmentAsignar.arguments = bundle
        return  fragmentAsignar
    }

    private fun readBundle(bundle: Bundle){
        title = bundle.getString("tipoError").toString()
        descripcion = bundle.getString("descripcion").toString()
        id = bundle.getString("idError").toString()
        idTicket = bundle.getString("idTicket").toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        netHelper = NetHelper(this)
        return inflater.inflate(R.layout.fragment_asignar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        netHelper.getDeptos()
        arguments?.let { readBundle(it) }

        asignar_button.setOnClickListener {
            if (activity?.isConnected()!!){
                if (selectedDepto != "")
                    netHelper.setAreaInError(selectedDepto, id, idTicket)
            }else{
                activity?.basicAlert(getString(R.string.no_internet), getString(R.string.no_internet_message))
            }

        }

    }

    override fun OnResultOk(any: Any, code: Int) {
        if (code == AppConstants.GET_DEPARTAMENTOS){

        }
        if (code == AppConstants.SET_AREA_IN_ERROR){
            showDialog(getString(R.string.confirmacion_asignado), getString(R.string.ticket_asignado))
        }
    }

    override fun OnError(error: GestionError, code: Int) {
        if (code == AppConstants.GET_DEPARTAMENTOS){
            activity?.window?.decorView?.let { showSnackbar(it, getString(error.error)) }

        }
    }

    override fun OnCollectionResult(results: MutableList<Any>, code: Int) {
        if (code == AppConstants.GET_DEPARTAMENTOS){
            deptosList = results as MutableList<Departamentos>
            deptosList.forEach {
                deptosListNombres.add(it.Nombre)
                deptosListIds.add(it.id)
            }
            if (deptosList.size > 0){
                // Seteamos los nombres de los departamentos al adapter
                val aa = activity?.let { ArrayAdapter(it, android.R.layout.simple_spinner_item, deptosListNombres) }
                // Seteamos el layout que usara el spinner, en este caso usamos uno por default
                aa?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Seteamos los datos al spinner
                area_spinner!!.setAdapter(aa)

                area_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                        selectedDepto = deptosListIds.get(position).toString()

                    }

                    override fun onNothingSelected(parent: AdapterView<*>){

                    }
                }
            }
        }
    }

    fun showDialog(message: String, title: String){
        val dialogBuilder = AlertDialog.Builder(activity!!)
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)
            .setPositiveButton(getString(R.string.continuar), DialogInterface.OnClickListener {
                    dialog, id ->
                dialog.dismiss()
                goToActivity()
            })

        val alert = dialogBuilder.create()
        alert.setTitle(title)
        alert.setCancelable(false)
        alert.show()
    }


    private fun goToActivity(){
        var intent = Intent(activity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}