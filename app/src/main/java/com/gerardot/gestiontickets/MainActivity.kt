package com.gerardot.gestiontickets

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.gerardot.gestiontickets.activities.TicketsList
import com.gerardot.gestiontickets.custom.ViewDialog
import com.gerardot.gestiontickets.model.Dashboard
import com.gerardot.gestiontickets.model.GestionError
import com.gerardot.gestiontickets.model.Ticket
import com.gerardot.gestiontickets.utils.*
import com.gerardot.gestiontickets.utils.AppConstants.TICKETS_ABIERTOS
import com.gerardot.gestiontickets.utils.AppConstants.TICKETS_ASIGNADOS
import com.gerardot.gestiontickets.utils.AppConstants.TICKETS_CERRADOS
import com.gerardot.gestiontickets.utils.AppConstants.TODOS_TICKETS
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Gerardo on 10/01/2020.
 */
class MainActivity : AppCompatActivity(), NetHelper.OnDataResultInterface, View.OnClickListener {

    private lateinit var netHelper: NetHelper
    private lateinit var dashboard: Dashboard

    private lateinit var viewDialog: ViewDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewDialog = ViewDialog(this)

        netHelper = NetHelper(this)

        viewDialog.showDialog()
        initView()
    }

    private fun initView(){
        //netHelper.getTickets()

        card_tickets_abiertos.setOnClickListener(this)
        card_tickets_cerrados.setOnClickListener(this)
        card_tickets_asignados.setOnClickListener(this)
        card_todos_tickets.setOnClickListener(this)
        //netHelper.listenForNewTickets()
    }

    override fun OnResultOk(any: Any, code: Int) {
        if (code == AppConstants.LISTEN_NEW_TICKETS){
            dashboard= any as Dashboard
            tickets_abiertos.text = getOneIntToText(dashboard.ticketsAbiertos)
            tickets_cerrados.text = getOneIntToText(dashboard.ticketsCerrados)
            tickets_asignados.text = getOneIntToText(dashboard.ticketsAsignados)
            todos_tickets.text = getOneIntToText(dashboard.todosTickets)
            viewDialog.hideDialog()
        }
    }

    override fun OnError(error: GestionError, code: Int) {
        if (code == AppConstants.LISTEN_NEW_TICKETS){
            showSnackbar(window.decorView, getString(error.error))
        }
    }

    override fun OnCollectionResult(results: MutableList<Any>, code: Int) {
        if (code == AppConstants.LISTEN_NEW_TICKETS){

        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.card_tickets_abiertos -> haveTicketsOpen(TICKETS_ABIERTOS)
            R.id.card_tickets_cerrados -> haveTicketsOpen(TICKETS_CERRADOS)
            R.id.card_tickets_asignados -> haveTicketsOpen(TICKETS_ASIGNADOS)
            R.id.card_todos_tickets -> haveTicketsOpen(TODOS_TICKETS)
        }
    }

    private fun haveTicketsOpen(seleccion: Int){
        if(seleccion == TICKETS_ABIERTOS && dashboard.ticketsAbiertos > 0){
            goToActivity(seleccion)
        }else if (seleccion == TICKETS_CERRADOS && dashboard.ticketsCerrados > 0){
            goToActivity(seleccion)
        }else if (seleccion == TICKETS_ASIGNADOS && dashboard.ticketsAsignados > 0){
            goToActivity(seleccion)
        }else if (seleccion == TODOS_TICKETS && dashboard.todosTickets > 0){
            goToActivity(seleccion)
        }else{
            showToast(getString(R.string.error_load_list), Toast.LENGTH_LONG)
        }
    }

    private fun goToActivity(seleccion: Int){
        if (isConnected()){
            var intent = Intent(this, TicketsList::class.java)
            intent.putExtra("seleccion", seleccion)
            startActivity(intent)
        }else{
            basicAlert(getString(R.string.no_internet), getString(R.string.no_internet_message))
        }
    }

    public override fun onStart() {
        super.onStart()
        netHelper.getTickets()
    }
}
