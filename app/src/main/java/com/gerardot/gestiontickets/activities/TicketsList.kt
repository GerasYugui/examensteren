package com.gerardot.gestiontickets.activities

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gerardot.gestiontickets.R
import com.gerardot.gestiontickets.adapters.TicketsAdapter
import com.gerardot.gestiontickets.custom.ViewDialog
import com.gerardot.gestiontickets.model.*
import com.gerardot.gestiontickets.utils.*
import kotlinx.android.synthetic.main.activity_tickets_list.*

/**
 * Created by Gerardo on 13/01/2020.
 */
class TicketsList : AppCompatActivity(), NetHelper.OnDataResultInterface, TicketsAdapter.OnItemClickListener {

    lateinit var mRecyclerView : RecyclerView
    val mAdapter : TicketsAdapter = TicketsAdapter()

    private lateinit var netHelper: NetHelper

    private var ticketsList: MutableList<Ticket> = ArrayList()
    private var ticketsFilterList: MutableList<Ticket> = ArrayList()

    private var deptosList: MutableList<Departamentos> = ArrayList()
    private var deptosListNombres: MutableList<String> = ArrayList()
    private var deptosListIds: MutableList<String> = ArrayList()

    private var empleadosList: MutableList<Empleado> = ArrayList()
    private var empleadosListNombres: MutableList<String> = ArrayList()
    private var empleadosListIds: MutableList<String> = ArrayList()

    private var tipo: Int = 0

    private lateinit var viewDialog: ViewDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tickets_list)

        viewDialog = ViewDialog(this)

        var bundle: Bundle ?= intent.extras
        tipo = bundle!!.getInt("seleccion", 0)

        netHelper = NetHelper(this)

        initView(tipo)

    }

    private fun initView(tipo: Int){
        viewDialog.showDialog()
        if (tipo == AppConstants.TICKETS_ABIERTOS || tipo == AppConstants.TICKETS_ASIGNADOS)
            area_spinner.visibility = View.VISIBLE
        else
            area_spinner.visibility = View.GONE
        netHelper.getTickets(tipo)
    }

    fun setUpRecyclerView(){
        mRecyclerView = findViewById<RecyclerView>(R.id.tickets_recycler)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mAdapter.TicketsAdapter(ticketsList, this,this)
        mRecyclerView.adapter = mAdapter

    }

    fun setUpRecyclerViewFilter(){
        mRecyclerView = findViewById<RecyclerView>(R.id.tickets_recycler)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mAdapter.TicketsAdapter(ticketsFilterList, this,this)
        mRecyclerView.adapter = mAdapter
    }

    private fun getErrorData(){
        var pos = -1
        ticketsList.forEach{
            pos = ++pos
            netHelper.getErrorById(it.idError, pos)
        }
    }

    private fun getEmpleadoData(){
        var pos = -1
        ticketsList.forEach{
            pos = ++pos
            if (it.AsignadoA != "")
                netHelper.getEmpleadoById(it.AsignadoA, pos)
        }
        getErrorData()
    }

    override fun OnResultOk(any: Any, code: Int) {
        if (code == AppConstants.GET_ERROR_BY_ID){
            var error = any as Error
            ticketsList[error.pos].error = error
            if (ticketsList.size == error.pos + 1){
                setUpRecyclerView()

                if (tipo == AppConstants.TICKETS_ABIERTOS)
                    netHelper.getDeptos()
                else if (tipo == AppConstants.TICKETS_ASIGNADOS)
                    netHelper.getEmpleados()
                else
                    viewDialog.hideDialog()
            }
        }
        if (code == AppConstants.GET_EMPLEADO_BY_ID){
            var empleado = any as Empleado
            ticketsList[empleado.pos].empleado = empleado
        }
    }

    override fun OnError(error: GestionError, code: Int) {
        if (code == AppConstants.GET_TICKETS){

        }
    }

    override fun OnCollectionResult(results: MutableList<Any>, code: Int) {
        if (code == AppConstants.GET_TICKETS){
            ticketsList = results as MutableList<Ticket>
            // obtenemos los datos de los errores
            getEmpleadoData()
        }
        if (code == AppConstants.GET_DEPARTAMENTOS){
            deptosList = results as MutableList<Departamentos>
            deptosListNombres.add("Todos")
            deptosList.forEach {
                deptosListNombres.add(it.Nombre)
                deptosListIds.add(it.id)
            }
            if (deptosList.size > 0){
                // Seteamos los nombres de los departamentos al adapter
                val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, deptosListNombres)
                // Seteamos el layout que usara el spinner, en este caso usamos uno por default
                aa?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Seteamos los datos al spinner
                area_spinner!!.setAdapter(aa)

                area_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                        // buscar los tickets que coincidan con el id del depto
                        if (position > 0){
                            if (ticketsList.size > 0){
                                ticketsFilterList.clear()
                                for (ticket in ticketsList){
                                    if (ticket.error?.areaAAsignar == deptosListIds[position-1]){
                                        ticketsFilterList.add(ticket)
                                    }
                                }
                                setUpRecyclerViewFilter()
                            }
                        }else
                            setUpRecyclerView()

                    }

                    override fun onNothingSelected(parent: AdapterView<*>){

                    }
                }
            }
            viewDialog.hideDialog()
        }
        if (code == AppConstants.GET_EMPLEADOS){
            empleadosList = results as MutableList<Empleado>
            empleadosListNombres.add("Todos")
            empleadosList.forEach {
                empleadosListNombres.add(it.Nombre)
                empleadosListIds.add(it.id)
            }

            if (empleadosList.size > 0){
                // Seteamos los nombres de los departamentos al adapter
                val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, empleadosListNombres)
                // Seteamos el layout que usara el spinner, en este caso usamos uno por default
                aa?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Seteamos los datos al spinner
                area_spinner!!.setAdapter(aa)

                area_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                        // buscar los tickets que coincidan con el id del depto
                        if (position > 0){
                            if (ticketsList.size > 0){
                                ticketsFilterList.clear()
                                for (ticket in ticketsList){
                                    if (ticket.AsignadoA == empleadosListIds[position-1]){
                                        ticketsFilterList.add(ticket)
                                    }
                                }
                                setUpRecyclerViewFilter()
                            }
                        }else
                            setUpRecyclerView()

                    }

                    override fun onNothingSelected(parent: AdapterView<*>){

                    }
                }
            }
            viewDialog.hideDialog()
        }
    }

    override fun onItemClicked(ticket: Ticket) {
        showCustomDialog(ticket)
    }

    private fun showCustomDialog(ticket: Ticket){

        var status = 0
        var viewGroup: ViewGroup = findViewById(android.R.id.content)
        var dialogView: View = LayoutInflater.from(this).inflate(R.layout.custom_ticket_dialog, viewGroup, false)

        var builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setView(dialogView)

        var alertDialog: AlertDialog = builder.create()
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        initComponents(dialogView, ticket, alertDialog, status)
        alertDialog.show()

    }

    private fun initComponents(dialogView: View, ticket: Ticket, alertDialog:AlertDialog, status: Int){
        val cerrarButton = dialogView.findViewById(R.id.cerrar_button) as Button
        val asignarButton = dialogView .findViewById(R.id.asignar_button) as Button
        val titulo = dialogView .findViewById(R.id.title) as TextView
        val statusView = dialogView .findViewById(R.id.status) as TextView
        val comentarios = dialogView .findViewById(R.id.comentarios) as TextView
        val asignadoA = dialogView .findViewById(R.id.asignado_a) as TextView
        val fechaAsignado = dialogView .findViewById(R.id.fecha_asignacion) as TextView
        val fechaCerrado = dialogView .findViewById(R.id.fecha_cierre) as TextView
        //val cerradoPor = dialogView .findViewById(R.id.cerrado_por) as TextView

        if (ticket.error?.areaAAsignar == ""){
            asignarButton.visibility = View.VISIBLE
        }else{
            asignarButton.visibility = View.GONE
        }

        titulo.text = ticket.error?.tipoError.toString()
        if (ticket?.fechaCierre!! > 0L)
            statusView.text = getString(R.string.cerrado)
        else
            statusView.text = getString(R.string.abierto)

        comentarios.text = getStringFormat(R.string.comentarios_custom, ticket.Comentarios)
        if (ticket.AsignadoA == "")
            asignadoA.text = getString(R.string.no_asignado)
        else
            asignadoA.text = getStringFormat(R.string.asignado_custom, ticket.empleado?.Nombre.toString())
        if (ticket?.fechaAsignacion!! > 0L)
            fechaAsignado.text = getStringFormat(R.string.fecha_asignado_custom, getDateString(ticket.fechaAsignacion))
        else
            fechaAsignado.text = getStringFormat(R.string.fecha_asignado_custom, getString(R.string.sin_fecha))
        if (ticket?.fechaCierre!! > 0L)
            fechaCerrado.text = getStringFormat(R.string.fecha_cierre_custom, getDateString(ticket.fechaCierre))
        else
            fechaCerrado.text = getStringFormat(R.string.fecha_cierre_custom, getString(R.string.sin_fecha))

        cerrarButton.setOnClickListener {
            alertDialog.dismiss()
        }

        asignarButton.setOnClickListener {
            ticket.error?.let { error ->  alertDialog.dismiss()
                error.idTicket = ticket.id
                goToActivity(error) }
        }

    }

    private fun goToActivity(error: Error){
        if (isConnected()){
            var intent = Intent(this, AsignarTicket::class.java)
            intent.putExtra("error", error)
            startActivity(intent)
        }else{
            basicAlert(getString(R.string.no_internet), getString(R.string.no_internet_message))
        }
    }


}
