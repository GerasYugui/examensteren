package com.gerardot.gestiontickets.activities

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.gerardot.gestiontickets.MainActivity
import com.gerardot.gestiontickets.R
import com.gerardot.gestiontickets.utils.AppConstants
import com.gerardot.gestiontickets.utils.basicAlert
import com.gerardot.gestiontickets.utils.isConnected

/**
 * Created by Gerardo on 10/01/2020.
 */
class SplashScreen : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    public override fun onStart() {
        super.onStart()
        Handler().postDelayed({
            if (isConnected()){
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }else{
                showDialog(getString(R.string.no_internet_message), getString(R.string.no_internet))
            }

        }, AppConstants.SPLASH_DELAY)
    }

    fun showDialog(message: String, title: String){
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)
            .setPositiveButton(getString(R.string.cerrar), DialogInterface.OnClickListener {
                    dialog, id ->
                dialog.dismiss()
                finish()
            })

        val alert = dialogBuilder.create()
        alert.setTitle(title)
        alert.setCancelable(false)
        alert.show()
    }
}
