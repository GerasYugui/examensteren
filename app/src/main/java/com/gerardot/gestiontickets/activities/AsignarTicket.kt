package com.gerardot.gestiontickets.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.gerardot.gestiontickets.R
import com.gerardot.gestiontickets.custom.ViewDialog
import com.gerardot.gestiontickets.fragments.FragmentAsignar
import com.gerardot.gestiontickets.fragments.FragmentPaso1
import com.gerardot.gestiontickets.fragments.FragmentPaso2
import com.gerardot.gestiontickets.fragments.FragmentPaso3
import com.gerardot.gestiontickets.model.Error
import com.gerardot.gestiontickets.model.Ticket
import kotlinx.android.synthetic.main.activity_asignar_ticket.*

/**
 * Created by Gerardo on 13/01/2020.
 */
class AsignarTicket : AppCompatActivity() {

    private lateinit var viewDialog: ViewDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asignar_ticket)

        // Recibimos el objeto de tipo error que mostraremos en pantalla y el cual sera asignado al area correspondiente
        var bundle: Bundle ?= intent.extras
        var error = bundle!!.getSerializable("error") as Error

        val adapter = MyViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(FragmentPaso1().newInstance(error) , getString(R.string.tab_text_1))
        adapter.addFragment(FragmentPaso2().newInstance(error) , getString(R.string.tab_text_2))
        adapter.addFragment(FragmentPaso3().newInstance(error) , getString(R.string.tab_text_3))
        adapter.addFragment(FragmentAsignar().newInstance(error) , getString(R.string.tab_text_4))
        viewPager.adapter = adapter
        tabs.setupWithViewPager(viewPager)
    }

    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager){

        private val fragmentList : MutableList<Fragment> = ArrayList()
        private val titleList : MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment,title:String){
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }

    }

}
