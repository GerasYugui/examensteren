package com.gerardot.gestiontickets.custom

import android.app.Activity
import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.ProgressBar
import com.gerardot.gestiontickets.R

/**
 * Created by Gerardo on 13/01/2020.
 */

class ViewDialog {

    private lateinit var activity: Activity
    private lateinit var dialog: Dialog


    constructor(activity:Activity){
        this.activity = activity
    }

    fun showDialog(){
        dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        dialog.setCancelable(false)
        dialog.setContentView(R.layout.loading_layout)

        var  gifImageView: ProgressBar = dialog.findViewById(R.id.custom_loading_imageView)

        dialog.show()

//        Flubber.with()
//            .animation(Flubber.AnimationPreset.SQUEEZE)
//            .repeatCount(100)
//            .duration(1000)
//            .createFor(gifImageView)
//            .start()
    }

    fun hideDialog(){
        dialog.dismiss()
    }

}