package com.gerardot.gestiontickets.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.gerardot.gestiontickets.R
import com.gerardot.gestiontickets.model.Ticket
import com.gerardot.gestiontickets.utils.getDateString

/**
 * Created by Gerardo on 13/01/2020.
 */
class TicketsAdapter : RecyclerView.Adapter<TicketsAdapter.ViewHolder>(){

    var tickets: MutableList<Ticket> = ArrayList()
    lateinit var context: Context
    lateinit var itemClickListener: OnItemClickListener

    fun TicketsAdapter(superheros : MutableList<Ticket>, context: Context, itemClickListener: OnItemClickListener){
        this.tickets = superheros
        this.context = context
        this.itemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketsAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_ticket, parent, false))
    }

    override fun getItemCount(): Int {
        return tickets.size
    }

    override fun onBindViewHolder(holder: TicketsAdapter.ViewHolder, position: Int) {
        val item = tickets.get(position)
        holder.bind(item, context, itemClickListener)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val tipoError = view.findViewById(R.id.tipo_error) as TextView
        val comentarios = view.findViewById(R.id.comentarios) as TextView
        val asignado = view.findViewById(R.id.asignado) as TextView
        val status = view.findViewById(R.id.status) as TextView
        val fechaAsignacion = view.findViewById(R.id.fecha_asignacion) as TextView
        val fechaCierre = view.findViewById(R.id.fecha_cierre) as TextView
        val cardView = view.findViewById(R.id.card_view) as CardView

        fun bind(ticket: Ticket, context: Context,itemClickListener: OnItemClickListener){
            tipoError.text = ticket.error?.tipoError.toString()
            comentarios.text = ticket.Comentarios
            if (ticket.AsignadoA == "")
                asignado.text = context.getString(R.string.no_asignado)
            else
                asignado.text = ticket.empleado?.Nombre
            if (ticket?.fechaCierre!! > 0L)
                status.text = context.getString(R.string.cerrado)
            else
                status.text = context.getString(R.string.abierto)
            if (ticket?.fechaAsignacion!! > 0L)
                fechaAsignacion.text = getDateString(ticket.fechaAsignacion)
            else
                fechaAsignacion.text = context.getString(R.string.sin_fecha)
            if (ticket?.fechaCierre!! > 0L)
                fechaCierre.text = getDateString(ticket.fechaCierre)
            else
                fechaCierre.text = context.getString(R.string.sin_fecha)

            cardView.setOnClickListener{
                itemClickListener.onItemClicked(ticket)
            }
        }

    }

    interface OnItemClickListener{
        fun onItemClicked(ticket: Ticket)
    }

}