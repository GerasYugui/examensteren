package com.gerardot.gestiontickets.model

/**
 * Created by Gerardo on 13/01/2020.
 */
data class Ticket (
    var AsignadoA: String,
    var Comentarios: String,
    var cerradoPor: String,
    var fechaAsignacion: Long,
    var fechaCierre: Long,
    var idError: String,
    var id: String,
    var error: Error?,
    var empleado: Empleado?

){
    constructor(): this("","","",0,0,"","",null,null)
}