package com.gerardot.gestiontickets.model

/**
 * Created by Gerardo on 13/01/2020.
 */
data class Empleado (
    var Nombre: String = "",
    var Usuario: String = "",
    var email: String = "",
    var idDepartamento: String = "",
    var password: String = "",
    var telefono: Long = 0,
    var pos: Int = -1,
    var id: String = ""
)