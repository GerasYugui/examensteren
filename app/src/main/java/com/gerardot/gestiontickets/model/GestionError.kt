package com.gerardot.gestiontickets.model

/**
 * Created by Gerardo on 13/01/2020.
 */
data class GestionError (
    var error: Int
)