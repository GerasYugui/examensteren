package com.gerardot.gestiontickets.model

import java.io.Serializable

/**
 * Created by Gerardo on 13/01/2020.
 */
data class Error (
    var Descripcion: String = "",
    var areaAAsignar: String = "",
    var paso1: String = "",
    var paso2: String = "",
    var paso3: String = "",
    var tipoError: Int = 0,
    var id: String  = "",
    var pos: Int = -1,
    var idTicket: String = ""
): Serializable