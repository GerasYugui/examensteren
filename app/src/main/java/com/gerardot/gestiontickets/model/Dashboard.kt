package com.gerardot.gestiontickets.model

/**
 * Created by Gerardo on 13/01/2020.
 */
data class Dashboard(
    var ticketsAbiertos: Int = 0,
    var ticketsCerrados: Int = 0,
    var todosTickets: Int  = 0,
    var ticketsAsignados: Int = 0
)