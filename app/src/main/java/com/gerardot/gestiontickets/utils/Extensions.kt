package com.gerardot.gestiontickets.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.text.format.DateFormat
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentActivity
import com.gerardot.gestiontickets.MainActivity
import com.gerardot.gestiontickets.R
import com.gerardot.gestiontickets.activities.TicketsList
import com.google.android.material.snackbar.Snackbar
import java.sql.Timestamp
import java.util.*

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT){
    Toast.makeText(this, message, duration).show()
}

fun showSnackbar(view: View, message: String, duration: Int = Snackbar.LENGTH_LONG){
    Snackbar.make(view, message, duration).setAction("Action", null).show()
}

fun Context.basicAlert(title: String, message: String){
    val builder : AlertDialog.Builder = AlertDialog.Builder(this)
    builder.setTitle(title)
    builder.setMessage(message)
    builder.setPositiveButton("Ok", DialogInterface.OnClickListener(function = dismissDialog))
    builder.show()
}

val dismissDialog = { dialog: DialogInterface, which: Int ->
    dialog.dismiss()
}

fun <T> Context.openActivity(it: Class<T>) {
    var intent = Intent(this, it)
    startActivity(intent)
}

fun getTimestamp(): Long{
    return Timestamp(System.currentTimeMillis()/1000).time
}

fun getDateString(timestamp: Long): String{
    var cal: Calendar = Calendar.getInstance(Locale.ENGLISH)
    cal.timeInMillis = timestamp * 1000
    var date: String = DateFormat.format("dd-MM-yyyy", cal).toString()
    return date
}

fun Context.isConnected(): Boolean{
    var connected = false
    val connec : ConnectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = connec.activeNetworkInfo
    connected = networkInfo != null && networkInfo.isConnected

    return connected
}

fun Context.getOneIntToText(numero: Int): String{
    var string = String.format(getString(R.string.contador), numero)
    return string
}

fun Context.getStringFormat(recurso: Int, mensaje: String): String{
    var string = String.format(getString(recurso), mensaje)
    return string
}