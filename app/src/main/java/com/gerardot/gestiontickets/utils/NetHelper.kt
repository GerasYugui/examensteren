package com.gerardot.gestiontickets.utils

import android.app.Service
import android.content.Context
import android.util.Log
import androidx.fragment.app.Fragment
import com.gerardot.gestiontickets.utils.AppConstants.LISTEN_NEW_TICKETS
import com.google.firebase.database.*
import com.gerardot.gestiontickets.R
import com.gerardot.gestiontickets.model.*
import com.gerardot.gestiontickets.utils.AppConstants.GET_DEPARTAMENTOS
import com.gerardot.gestiontickets.utils.AppConstants.GET_EMPLEADOS
import com.gerardot.gestiontickets.utils.AppConstants.GET_EMPLEADO_BY_ID
import com.gerardot.gestiontickets.utils.AppConstants.GET_ERROR_BY_ID
import com.gerardot.gestiontickets.utils.AppConstants.GET_TICKETS
import com.gerardot.gestiontickets.utils.AppConstants.TICKETS_ABIERTOS
import com.gerardot.gestiontickets.utils.AppConstants.TICKETS_ASIGNADOS
import com.gerardot.gestiontickets.utils.AppConstants.TICKETS_CERRADOS
import com.gerardot.gestiontickets.utils.AppConstants.TODOS_TICKETS
import java.lang.Boolean

class NetHelper (){

    private val DEVELOPMENT_PATH: String = "development"
    private val RELEASE_PATH: String = "production"

    val EMPLEADOS_PATH: String = "Empleados"
    val DEPARTAMENTOS_PATH: String = "Departamentos"
    val ERROR_PATH: String = "Error"
    val TICKET_PATH: String = "Ticket"
    val CHILD_ERROR_AREA: String  = "areaAAsignar"
    val CHILD_TICKET_FECHA_ASIGNACION: String  = "fechaAsignacion"

    interface OnDataResultInterface{
        fun OnResultOk(any: Any, code: Int)
        fun OnError(error: GestionError, code: Int)
        fun OnCollectionResult(results: MutableList<Any>, code: Int)
    }

    private lateinit var onDataResultInterface: OnDataResultInterface

    fun setOnDataResultInterface(onDataResultInterface: OnDataResultInterface){
        this.onDataResultInterface = onDataResultInterface
    }

    constructor(context: Context): this(){
        onDataResultInterface = context as OnDataResultInterface
    }

    constructor(context: Fragment): this(){
        onDataResultInterface = context as OnDataResultInterface
    }

    constructor(context: Service): this(){
        onDataResultInterface = context as OnDataResultInterface
    }

    fun getPathMode(): String{
        if (AppConstants.DEBUG){
            return DEVELOPMENT_PATH
        }else{
            return RELEASE_PATH
        }
    }

    fun listenForNewTickets(){
        FirebaseDatabase.getInstance().getReference(getPathMode()).child(TICKET_PATH)
            .addChildEventListener(object : ChildEventListener {
                override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                    var ticket = p0.getValue(Ticket::class.java)
                    ticket?.id ?: p0.key.toString()
                    onDataResultInterface.OnResultOk(ticket as Any, LISTEN_NEW_TICKETS)
                }

                override fun onChildChanged(p0: DataSnapshot, p1: String?) {

                }

                override fun onChildRemoved(p0: DataSnapshot) {

                }

                override fun onChildMoved(p0: DataSnapshot, p1: String?) {

                }

                override fun onCancelled(p0: DatabaseError) {
                    val error = GestionError(R.string.error_get_tickets)
                    onDataResultInterface.OnError(error, LISTEN_NEW_TICKETS)
                }
            })
    }

    fun getTickets(){
        FirebaseDatabase.getInstance().getReference(getPathMode()).child(TICKET_PATH)
            .addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()){
                        if (snapshot.hasChildren()){
                            var dashboard = Dashboard()
                            var tickets = snapshot.children
                            tickets.forEach{

                                val ticket = it.getValue(Ticket::class.java)
                                ticket?.id = it.key.toString()
                                dashboard.todosTickets = dashboard.todosTickets + 1 // optenemos todos los tickets

                                // contar tickets abiertos
                                if (ticket?.fechaCierre!! > 0L){
                                    dashboard.ticketsCerrados = dashboard.ticketsCerrados + 1
                                }else{
                                    dashboard.ticketsAbiertos = dashboard.ticketsAbiertos + 1
                                }

                                if (ticket?.AsignadoA != ""){
                                    dashboard.ticketsAsignados = dashboard.ticketsAsignados + 1
                                }

                            }
                            onDataResultInterface.OnResultOk(dashboard as Any, LISTEN_NEW_TICKETS)
                        }

                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    val error = GestionError(R.string.error_get_tickets)
                    onDataResultInterface.OnError(error, LISTEN_NEW_TICKETS)
                }
            })
    }

    fun getTickets(tipo: Int){
        FirebaseDatabase.getInstance().getReference(getPathMode()).child(TICKET_PATH)
            .addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()){
                        if (snapshot.hasChildren()){
                            var ticketList :MutableList<Any> = ArrayList()
                            var tickets = snapshot.children
                            tickets.forEach{

                                val ticket = it.getValue(Ticket::class.java)
                                ticket?.id = it.key.toString()

                                // contar tickets abiertos
                                if (ticket?.fechaCierre!! > 0L && tipo == TICKETS_CERRADOS){
                                    ticketList.add(ticket)
                                }else if (ticket?.fechaCierre!! == 0L && tipo == TICKETS_ABIERTOS){
                                    ticketList.add(ticket)
                                }else if (ticket?.AsignadoA != "" && tipo  == TICKETS_ASIGNADOS){
                                    ticketList.add(ticket)
                                }else if (tipo == TODOS_TICKETS){
                                    ticketList.add(ticket)
                                }

                            }
                            onDataResultInterface.OnCollectionResult(ticketList, GET_TICKETS)
                        }

                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    val error = GestionError(R.string.error_get_tickets)
                    onDataResultInterface.OnError(error, GET_TICKETS)
                }
            })
    }

    fun getErrorById(id: String, pos: Int){
        FirebaseDatabase.getInstance().getReference(getPathMode()).child(ERROR_PATH).child(id)
            .addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()){
                        var error = snapshot.getValue(Error::class.java)
                        error?.id = snapshot.key.toString()
                        error?.pos = pos
                        onDataResultInterface.OnResultOk(error as Any, GET_ERROR_BY_ID)
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    val error = GestionError(R.string.error_get_error)
                    onDataResultInterface.OnError(error, GET_ERROR_BY_ID)
                }
            })
    }

    fun getEmpleadoById(id: String, pos: Int){
        FirebaseDatabase.getInstance().getReference(getPathMode()).child(EMPLEADOS_PATH).child(id)
            .addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()){
                        var empleado = snapshot.getValue(Empleado::class.java)
                        empleado?.pos = pos
                        onDataResultInterface.OnResultOk(empleado as Any, GET_EMPLEADO_BY_ID)
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    val error = GestionError(R.string.error_get_empleados)
                    onDataResultInterface.OnError(error, GET_EMPLEADO_BY_ID)
                }
            })
    }

    fun getDeptos(){
        FirebaseDatabase.getInstance().getReference(getPathMode()).child(DEPARTAMENTOS_PATH)
            .addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()){
                        if (snapshot.hasChildren()){
                            var deptosList :MutableList<Any> = ArrayList()
                            var deptos = snapshot.children
                            deptos.forEach{

                                val depto = it.getValue(Departamentos::class.java)
                                depto?.id = it.key.toString()
                                depto?.let { it1 -> deptosList.add(it1) }

                            }
                            onDataResultInterface.OnCollectionResult(deptosList, GET_DEPARTAMENTOS)
                        }

                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    val error = GestionError(R.string.error_get_deptos)
                    onDataResultInterface.OnError(error, GET_DEPARTAMENTOS)
                }
            })
    }

    fun setAreaInError(deptoId: String, errorId: String, ticketId: String){
        FirebaseDatabase.getInstance().getReference(getPathMode()).child(ERROR_PATH).child(errorId).child(CHILD_ERROR_AREA).setValue(deptoId)
            .addOnCompleteListener { task ->
                if (task.isComplete && task.isSuccessful){
                    FirebaseDatabase.getInstance().getReference(getPathMode()).child(TICKET_PATH).child(ticketId).child(CHILD_TICKET_FECHA_ASIGNACION).setValue(
                        getTimestamp()).addOnCompleteListener { task ->
                        if (task.isSuccessful)
                            onDataResultInterface.OnResultOk(Boolean.TRUE, AppConstants.SET_AREA_IN_ERROR)
                        else{
                            val error = GestionError(R.string.error_set_area_in_depto)
                            onDataResultInterface.OnError(error, AppConstants.SET_AREA_IN_ERROR)
                        }
                    }

                }else{
                    val error = GestionError(R.string.error_set_area_in_depto)
                    onDataResultInterface.OnError(error, AppConstants.SET_AREA_IN_ERROR)
                }
            }
    }

    fun getEmpleados(){
        FirebaseDatabase.getInstance().getReference(getPathMode()).child(EMPLEADOS_PATH)
            .addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()){
                        if (snapshot.hasChildren()){
                            var empleadosList :MutableList<Any> = ArrayList()
                            var empleados = snapshot.children
                            empleados.forEach{

                                val empleado = it.getValue(Empleado::class.java)
                                empleado?.id = it.key.toString()
                                empleado?.let { it1 -> empleadosList.add(it1) }

                            }
                            onDataResultInterface.OnCollectionResult(empleadosList, GET_EMPLEADOS)
                        }

                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    val error = GestionError(R.string.error_get_deptos)
                    onDataResultInterface.OnError(error, GET_EMPLEADOS)
                }
            })
    }

}