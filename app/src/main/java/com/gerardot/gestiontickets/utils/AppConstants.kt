package com.gerardot.gestiontickets.utils

/**
 * Created by Gerardo on 10/01/2020.
 */

object AppConstants{
    val SPLASH_DELAY : Long = 3000
    val DEBUG = true
    val LISTEN_NEW_TICKETS = 1
    val GET_TICKETS = 2
    val GET_ERROR_BY_ID = 3
    val GET_EMPLEADO_BY_ID = 4
    val GET_DEPARTAMENTOS = 5
    val GET_EMPLEADOS = 7
    val SET_AREA_IN_ERROR = 6
    val TICKETS_ABIERTOS = 1
    val TICKETS_CERRADOS = 2
    val TICKETS_ASIGNADOS = 4
    val TODOS_TICKETS = 3
}